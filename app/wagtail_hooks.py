from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register, ModelAdminGroup

from .models import Section, Subsection


class SectionAdmin(ModelAdmin):
    model = Section
    menu_label = "Секция"
    menu_icon = "pick"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("name", "has_basket")
    list_filter = ("has_basket",)
    search_fields = ("name",)


class SubsectionAdmin(ModelAdmin):
    model = Subsection
    menu_label = "Подсекция"
    menu_icon = "pick"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("name", "section")
    list_filter = ("section",)
    search_fields = ("name",)


class SectionGroup(ModelAdminGroup):
    menu_label = "Секции"
    menu_icon = "pick"
    menu_order = 500
    items = (SectionAdmin, SubsectionAdmin)


modeladmin_register(SectionGroup)
