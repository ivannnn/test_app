from django.db import models


# Create your models here.

class Section(models.Model):
    name = models.CharField(max_length=64, verbose_name="Название")
    has_basket = models.BooleanField(default=False, verbose_name="Имеет корзину")
    logo = models.ImageField(upload_to='sections', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Секция"
        verbose_name_plural = "Секции"


class Subsection(models.Model):
    name = models.CharField(max_length=64, verbose_name="Название")
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='subsections', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Подсекция"
        verbose_name_plural = "Подсекции"
