### ```app``` - Обычное *django* приложение
#### ```app/wagtail_hooks.py``` - Регистрация обычных моделей в *wagtail* админке

### ```pages```  - Приложение с использованием *wagtail*
#### ```pages/models.py``` - Модели страниц *wagtail*

```
class AboutPage(Page):
    body = RichTextField(blank=True, verbose_name="Содержание")
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, blank=True, null=True
    )

    # Динамическое поле состоит из блоков
    # Есть набор стандартных блоков, но можно создать свои (Наследоватmься от Block)
    content = StreamField([
            ('heading', blocks.CharBlock(classname="full title")),
            ('paragraph', blocks.RichTextBlock()),
            ('image', ImageChooserBlock()),
        ], null=True, blank=True)

    # То что будет выводиться в админку
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        ImageChooserPanel('image'),
        StreamFieldPanel('content')
    ]
    # То что будет выводиться в API
    api_fields = [
        APIField('title'),
        APIField('body'),
        APIField('image'),
        APIField('content'),
    ]
```

### ```pages/api.py``` -  регистрация роутера для *wagtail* (добавляеться в главный роутер приложений ```test_app/urls.py```)
#### [Официальная документация](https://docs.wagtail.io/en/v2.9/advanced_topics/api/v2/configuration.html) по подключению апи
#### [Официальная документация](https://docs.wagtail.io/en/v2.9/getting_started/integrating_into_django.html) по внедрению *wagtail* в *django* проект
