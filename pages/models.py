from django.db import models
from wagtail.api import APIField

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks

class HomePage(Page):
    body = RichTextField(blank=True, verbose_name="Содержание")

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]
    api_fields = [
        APIField('title'),
        APIField('body'),
    ]


class AboutPage(Page):
    body = RichTextField(blank=True, verbose_name="Содержание")
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, blank=True, null=True
    )
    content = StreamField([
        ('heading', blocks.CharBlock(classname="full title")),
        ('paragraph', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
    ], null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        ImageChooserPanel('image'),
        StreamFieldPanel('content')
    ]

    api_fields = [
        APIField('title'),
        APIField('body'),
        APIField('image'),
        APIField('content'),
    ]
